<?php

namespace AuthBundle\Services;

use AuthBundle\Exception\UserUnauthorizedException;
use AuthBundle\Services\AccountStatus\LockedAccountStatus;
use AuthBundle\Services\AccountStatus\BannedAccountStatus;
use AuthBundle\Services\AccountStatus\AccountStatusModel;
use AuthBundle\Security\User\WebserviceUser;
use AuthBundle\Services\WsUserManager;

/**
 * Description of AccountStatusManager
 *
 * @author NachoBordon
 */
class AccountStatusManager 
{
    /**
     *
     * @var WsUserManager
     */
    private $userManager;
     
    /**
     * 
     * @param WsUserManager $userManager
     */
    public function __construct(
        WsUserManager $userManager
    ){
        $this->userManager = $userManager;
    }
    
    public function checkValidAccount(WebserviceUser $userData)
    {        
        //$accountStatus = $this->userManager->getAccountStatus( $userData->getUEIgamer() );
        $accountStatus = $userData->getAccountStatus();
        
        /*if ( !isset($accountStatus['code']) ) { 
            $accountStatus = ['code' => 'VisitorAccountStatus'];
        }*/

        $accountModel = new AccountStatusModel($accountStatus);
                
        if ( $accountModel->status instanceof LockedAccountStatus || $accountModel->status instanceof BannedAccountStatus ) {
            throw new UserUnauthorizedException(
                $accountModel->status->getCode(),
                sprintf("User '%s' unauthorized.", $userData->getUsername() )
            );
        }

    }
}
