<?php

namespace AuthBundle\Services\TimeoutHandler;

use Symfony\Component\HttpFoundation\ServerBag;
use Symfony\Component\Yaml\Parser;
use Lsw\MemcacheBundle\Doctrine\Cache\MemcacheCache;
use Lsw\MemcacheBundle\Cache\AntiDogPileMemcache;


class TimeoutHandler
{
    const TIME_LOGIN_TOKEN = 3600;

    // MANEJAMOS CLAVES POR user, navegador, ip

//    private $username = 'username'; // define default username var on request
//    private $timeouts = array("ip_human" => 10, "ip_bot" => 2, "user_human" => 10, "cookie_human" => 10); // define expire timeout counter for diferent tests
//    private $tops2ban = array("ip_human" => 20, "ip_bot" => 10, "user_human" => 3, "cookie_human" => 3);  // define max number of false login after timed ban
//    private $tpenalty = array("ip_human" => 100, "ip_bot" => 3600, "user_human" => 100, "cookie_human" => 100); // define time of penal ban
    private $key      = array();   // define keys of requested test

    private $timeouts = array();
    private $tops2ban = array();
    private $tpenalty = array();

    /** @var \Memcache  */
    private $server;
    private $user;
    /** @var  ServerBag */
    private $requestServer;
    /** @var MemcacheCache */
    //private $memcache;
    

    public function __construct(AntiDogPileMemcache $memcache)
    {
        $this->server = $memcache;
        
        $this->timeouts = $this->getConfig('timeouts');
        $this->tops2ban = $this->getConfig('tops2ban');
        $this->tpenalty = $this->getConfig('tpenalty');

//        $this->server = new \Memcache();
//        $this->server->connect($memcache['host'], $memcache['port']) or die ("No se pudo contectar a memcached".json_encode($memcache));
    }

    /**
     * @param $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function setRequestServer($server)
    {
        $this->requestServer = $server;
    }

    /**
     * @param string $key
     */
    private function getConfig($key)
    {
        $yaml = new Parser();
        $read = $yaml->parse(file_get_contents(__DIR__.'/config.yml'));

        return $read[$key];
    }

    /**
     *
     * @return bool
     */
    public function isValid()
    {
        $this->buildKeys();

        foreach ($this->key as $value) {
            if ($this->test_if_banned_key($value)) {
                return false;
            }
        }

        return true;
    }

    /*
     *  Invoke when login failed
     *
     */
    public function setLoginCrash()
    {
        $this->buildKeys(); // make a request keys

        foreach ($this->key as $key => $value) {
            $try = $this->server->get("TRY_".$value); // get Try number for this key
            $try = ($try == false) ? 1 : $try + 1 ; // inc number of try.
            $this->server->set("TRY_".$value,$try,0,$this->timeouts[$key]); // set new timeout for this key
            if ($try >= $this->tops2ban[$key]) {
                $this->server->set("BAN_".$value,true,0,$this->tpenalty[$key]); // if number of try for this key is mayor or eq of configured on tops2ban, set new ban timeout for this key
            }
        }
    }

    /**
     * @param $key
     *
     * @return array|string
     */
    private function test_if_banned_key($key)
    {
        return $this->server->get("BAN_".$key);
    }

    /**
     * set array key.
     */
    private function buildKeys()
    {
        $key['ip_human'] = "ih_".md5($this->requestServer->get('REMOTE_ADDR'));
        $key['ip_bot'] = "ib_".md5($this->requestServer->get('REMOTE_ADDR'));
        $key['user_human'] = "us_".md5($this->user);
        if ($this->requestServer->has('HTTP_COOKIE')) {
            $key['cookie_human']= "co_".md5($this->requestServer->get('HTTP_COOKIE'));
        }

        $this->key = $key;
    }
    
    /**
     * 
     * @return array
     */
    public function getRemainingAttempts()
    {
        $this->buildKeys(); // make a request keys
        $attempts = array();
        
        foreach ($this->key as $key => $value) {
            $try = $this->server->get("TRY_".$value); // get Try number for this key
            if ($try == false) { $try = 0; } // inc number of try.
            $attempts[$key] = $this->tops2ban[$key] - $try;
        }
        
        return $attempts;
    }

    /**
     * test login token is valid
     *
     * @param  string        $token
     * @return array|boolean
     */
    public function testLoginToken($token)
    {
        return $this->server->get($token);

    }

    /*
     *  Invoke when token is not valid
     *
     */
    public function setTokenLoginCrash()
    {
        $this->buildKeys(); // make a request keys

        foreach ($this->key as $value) {
            $this->server->set("BAN_".$value, true, 0, self::TIME_LOGIN_TOKEN); //set new ban timeout for this key
        }

    }
}
