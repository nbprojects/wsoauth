<?php

namespace AuthBundle\Services;

use Doctrine\ORM\EntityManager;
use AuthBundle\Entity\AccessToken;
use Texyon\Managers\Lib\wsReportManager;

/**
 * Description of TokenManager
 *
 * @author Ignacio
 */
class TokenManager
{
    /** @var EntityManager */ 
    private $em;
    private $access_token;
    private $refresh_token;
    private $expires_in;
    private $scope;
    
    
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;

    }
    
    public function load(array $config)
    {        
        $conf = $this->completeConfigKeys($config);
        
        $this->access_token = $conf['access_token'];
        $this->expires_in = $conf['expires_in'];
        $this->token_type = $conf['token_type'];
        $this->scope = $conf['scope'];
        $this->refresh_token = $conf['refresh_token'];
        
        return $this;
    }
            
    private function completeConfigKeys($config)
    {
        $defaultConfig = [
            'access_token' => null,
            'expires_in' => null,
            'token_type' => null,
            'scope' => null,
            'refresh_token' => null
        ];
        
        return array_merge($defaultConfig, $config);
    }
    
    public function setUser(AccessToken $accessToken)
    {       
        //$username = $accessToken->getUser()->getUsername();
        //$accessToken->setUsername($username);
        //$accountId = $this->getAccountByUsername($username);
        $accessToken->setAccount($accountId);
        $this->entitySave($accessToken);
    }
    
    private function getAccountByUsername($username)
    {
        
    }
    
    private function entitySave($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }
}
