<?php

namespace AuthBundle\Services;

use AuthBundle\Services\AccountStatusManager;
use AuthBundle\Services\DeviceManager;
use AuthBundle\Exception\BadCredentialsException;
use AuthBundle\Services\TimeoutHandler\TimeoutHandler;
use FOS\OAuthServerBundle\Storage\OAuthStorage;
use OAuth2\OAuth2;
use Symfony\Component\HttpFoundation\Request;
use OAuth2\OAuth2ServerException;


/**
 * Description of GrantAccess
 *
 */
class GrantAccess 
{
    /** @var OAuthStorage */
    protected $storage;
    /** @var AccountStatusManager  */
    protected $accountStatusManager;
    /** @var DeviceManager  */
    protected $deviceManager;
    /** @var  */
    private $client;
    /** @var TimeoutHandler */
    private $timeoutHandler;
    /** @var OAuth2 */
    private $server;


    public function __construct(
        AccountStatusManager $accountStatusManager,
        DeviceManager $deviceManager,
        TimeoutHandler $timeoutHandler
    )
    {
        $this->accountStatusManager = $accountStatusManager;
        $this->deviceManager = $deviceManager;
        $this->timeoutHandler = $timeoutHandler;
    }
    
    public function setServer(OAuth2 $server)
    {
        $this->server = $server;
    }
    
    /**
     * @return \FOS\OAuthServerBundle\Storage\OAuthStorage
     */
    public function getOAuthStorage()
    {
        $reflection = new \ReflectionClass($this->server);
        $storage = $reflection->getProperty('storage');
        $storage->setAccessible(true);
        $this->storage = $storage->getValue($this->server);
    }
    
    public function checkAccess(Request $request)
    {
        try{            
            if ($request->getMethod() === 'POST') {
                $inputData = $request->request->all();
            } else {
                $inputData = $request->query->all();
            }
            $locale = isset($inputData['locale'])? $inputData['locale'] : 'en';
        
            $this->setTimeoutOptions($inputData['username'], $request->server);
            
            if(false === $this->timeoutHandler->isValid()){
                throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, OAuth2::ERROR_INVALID_GRANT, "User is temporarily banned, for there are too many username/password input.");
            }
            
            
            $this->getOAuthStorage();    
            $this->client = $this->getClient($inputData['client_id']);
            $clientSecret = isset($inputData['client_secret'])? $inputData['client_secret'] : null;

            if (!$this->storage->checkClientCredentials($this->client, $clientSecret) ) {
                throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, OAuth2::ERROR_INVALID_CLIENT, 'The client credentials are invalid');
            }

            $stored = $this->storage->checkUserCredentials($this->client, $inputData['username'], $inputData['password']);

            if (!$stored) {
                $this->timeoutHandler->setLoginCrash();                
                throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, OAuth2::ERROR_INVALID_GRANT, "Invalid username and password combination");
            }

            $user = $stored['data']; // user

            //TODO: ÑAPA HASTA QUE CONECTEMOS EL wsTOOLS CON EL UEI Y EL wsUser PARA LOS ESTADOS DE LA CUENTA
            if( $user->getUEIgamer() ){
                $this->accountStatusManager->checkValidAccount($user);
            }

            $this->deviceManager->checkLocked( $this->client ); 
            $this->deviceManager->checkAccountLock( $this->client, $user, $inputData['username'], $locale); 
   
            if(!$clientSecret){
                $this->deviceManager->checkUnusualDevice( $this->client, $user, $request->getClientIp(), $inputData['username'], $locale);
            }
            
            return $user;

        }catch (OAuth2ServerException $e) {
            $this->accessException($e);
        }
        
    }
    
    private function getClient($client_id)
    {
        $client = $this->storage->getClient( $client_id ); 

        if(!$client){
            throw new OAuth2ServerException(OAuth2::HTTP_BAD_REQUEST, OAuth2::ERROR_INVALID_GRANT, 'The client credentials are invalid');
        }
        
        return $client;
    }
    
    
    private function setTimeoutOptions($username, $server) 
    {
        $this->timeoutHandler->setUser($username);
        $this->timeoutHandler->setRequestServer($server);        
    }


    private function accessException(OAuth2ServerException $e)
    {
        $unauthorizedCodes = ['invalid_grant', 'invalid_client'];
        $response = json_decode($e->getResponseBody(), true);

        if( in_array($response['error'], $unauthorizedCodes) ){

            throw new BadCredentialsException($e->getCode(), $e->getDescription());
        }

        throw $e;
    }  
    
    
}
