<?php


namespace AuthBundle\Services\AccountStatus;

use AuthBundle\Services\AccountStatus\IAccountStatus;
use AuthBundle\Services\AccountStatus\VisitorAccountStatus;
use AuthBundle\Services\AccountStatus\AccountStatusBuilder;
use AuthBundle\Services\AccountStatus\AccountStatus;

/**
 * Description of AccountStatusModel
 *
 * @author NachoBordon
 */
class AccountStatusModel {
    
    /**
     *
     * @var AccountStatus
     */
    public $status;

    public function __construct($code = null) {
        $this->status = new VisitorAccountStatus($this->status);
        
        if ($code != null) {
            $statusBuilder = new AccountStatusBuilder($this->status);
            $this->status = $statusBuilder->create($code);
        } 
    }
    
}
