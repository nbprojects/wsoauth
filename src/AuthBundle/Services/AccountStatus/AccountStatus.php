<?php


namespace AuthBundle\Services\AccountStatus;

use AuthBundle\Services\AccountStatus\IAccountStatus;

/**
 * Description of AccountStatus
 *
 * @author NachoBordon
 */
abstract class AccountStatus implements IAccountStatus
{
    /** $status IAccountStatus */
    protected $status;
    
            
    /**
     * 
     * @param IAccountStatus $status
     */
    function __construct(&$status)
    {
        $this->status = $status;
    }

    function getCode()
    {        
        return (new \ReflectionClass($this))->getShortName();
    }

    function save($accountId)
    {
        $code = $this->getCode();
        //$this->accountStatusModel->create($accountId, $code);
        //persist status
    }


    abstract function Register();
    abstract function Validate();
    abstract function Lock();
    abstract function Unlock();
    abstract function Bann();
    abstract function Suscriber();
    abstract function UnSuscriber();
    
}
