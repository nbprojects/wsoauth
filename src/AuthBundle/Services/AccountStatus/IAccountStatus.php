<?php

namespace AuthBundle\Services\AccountStatus;

/**
 *
 * @author NachoBordon
 */
interface IAccountStatus {    

    function Register();

    function Suscriber();

    function UnSuscriber();

    function Validate();

    function Lock();

    function Unlock();

    function Bann();

}
