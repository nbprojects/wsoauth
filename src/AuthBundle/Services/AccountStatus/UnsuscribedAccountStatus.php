<?php


namespace AuthBundle\Services\AccountStatus;

use AuthBundle\Services\AccountStatus\AccountStatus;

/**
 * Description of UnsuscribedAccountStatus
 *
 * @author NachoBordon
 */
class UnsuscribedAccountStatus extends AccountStatus
{
    //put your code here
    public function Bann() {
        
    }

    public function Lock() {
        
    }

    public function Register() {
        
    }

    public function Suscriber() {
        
    }

    public function UnSuscriber() {
        
    }

    public function Unlock() {
        
    }

    public function Validate() {
        
    }

}
