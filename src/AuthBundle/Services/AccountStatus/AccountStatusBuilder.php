<?php


namespace AuthBundle\Services\AccountStatus;

use AuthBundle\Services\AccountStatus\IAccountStatus;

/**
 * Description of AccountStatusBuilder
 *
 * @author NachoBordon
 */
class AccountStatusBuilder
{
    const ACCOUNT_STATUS_NAMESPACE = '\AuthBundle\Services\AccountStatus\\';
    /** $status IAccountStatus */
    protected $context;
    protected $code;
    protected $createdAt;

            
    function __construct(IAccountStatus $context)
    {
        $this->context = $context;
    }

    function create($code)
    {        
        $statusClass =  self::ACCOUNT_STATUS_NAMESPACE . $code;
        return new $statusClass($this->context);
    }

}
