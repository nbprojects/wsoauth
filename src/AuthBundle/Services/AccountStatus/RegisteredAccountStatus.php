<?php

namespace AuthBundle\Services\AccountStatus;

use AuthBundle\Services\AccountStatus\AccountStatus;
use Http\Discovery\Exception\NotFoundException;
use AuthBundle\Services\AccountStatus\BannedAccountStatus;
use AuthBundle\Services\AccountStatus\LockedAccountStatus;
use AuthBundle\Services\AccountStatus\ValidatedAccountStatus;
use AuthBundle\Services\AccountStatus\UnsuscribedAccountStatus;


/**
 * Description of RegisteredAccountStatus
 *
 * @author NachoBordon
 */
class RegisteredAccountStatus extends AccountStatus 
{

    
    public function Bann() {
        $this->status = new BannedAccountStatus($this->status);
    }

    public function Lock() {
        $this->status = new LockedAccountStatus($this->status);
    }

    public function Register() {
        throw new NotFoundException();
    }

    public function Suscriber() {
        throw new NotFoundException();
    }

    public function UnSuscriber() {
        $this->status = new UnsuscribedAccountStatus($this->status);
    }

    public function Unlock() {
        throw new NotFoundException();
    }

    public function Validate() {
        $this->status = new ValidatedAccountStatus($this->status);
    }

}
