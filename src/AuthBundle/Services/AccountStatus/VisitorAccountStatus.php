<?php

namespace AuthBundle\Services\AccountStatus;

use AuthBundle\Services\AccountStatus\AccountStatus;
use AuthBundle\Services\AccountStatus\RegisteredAccountStatus;
use Http\Discovery\Exception\NotFoundException;

/**
 * Description of VisitorAccountStatus
 *
 * @author NachoBordon
 */
class VisitorAccountStatus extends AccountStatus
{
    
    public function Bann() {
        throw new NotFoundException();
    }

    public function Lock() {
        throw new NotFoundException();
    }

    public function Register() {
        $this->status = new RegisteredAccountStatus($this->status);
        $this->save($accountId);
    }

    public function Suscriber() {
        throw new NotFoundException();
    }

    public function UnSuscriber() {
        throw new NotFoundException();
    }

    public function Unlock() {
        throw new NotFoundException();
    }

    public function Validate() {
        throw new NotFoundException();
    }

}
