<?php

namespace AuthBundle\Services;

use AuthBundle\Entity\ClientAccount;
use AuthBundle\Repository\ClientAccountRepository;

/**
 * 
 * Description of AccountManager
 *
 */
class AccountManager 
{
    /** @var ClientAccountRepository  */
    private $accountsLockRepository;
    
        
    public function __construct(
        ClientAccountRepository $accountsLockRepository
    ) 
    {   
        $this->accountsLockRepository = $accountsLockRepository;
    }
    
    
    /**
     * 
     * @param int    $accountId
     * @param string $device
     * @param string $ipClient
     * @return type
     */
    public function isKnownDevice($accountId, $device, $ipClient)
    {           
        if( $this->isFirstDevice($accountId) ) {
            $this->createKnownDevice($accountId, $device, $ipClient);
            
            return true;
        }
  
        $accountsLock = $this->accountsLockRepository->findAccountLockUnlockedDevice($accountId, $device)->getOneOrNullResult();  
        $known = ($accountsLock instanceof ClientAccount);
       
        return $known;
    }
    
    public function isFirstDevice($accountId)
    {
        $accountsLock = $this->accountsLockRepository->findByAccount($accountId);

        if( empty($accountsLock) ){
            return true;
        }
        
        return false;
    }


    /**
     * 
     * @param int $accountId
     * @param string $device
     * @return type
     */
    public function isLockedAccountDevice($accountId, $device)
    {           
        $accountsLock = $this->accountsLockRepository->findAccountLockByDevice($accountId, $device)
                ->getOneOrNullResult();
        
        if($accountsLock instanceof ClientAccount){
            return [
                'locked' => true,
                'unlockCode' => $accountsLock->getUnlockCode() 
            ];
        }
       
        return [
            'locked' => false
        ];
    }
    
    /**
     * 
     * @param int $accountId
     * @param string $device
     * @param string $ipClient
     * @return type
     */
    public function createKnownDevice($accountId, $device, $ipClient)
    {   
        $accountLock = new ClientAccount();
        $accountLock->newKnownDevice($accountId, $device, $ipClient);
        $this->accountsLockRepository->save($accountLock);
                
        return $accountLock;
    }
    
    /**
     * 
     * @param int $accountId
     * @param string $device
     * @param string $ipClient
     * @return type
     */
    public function lockAccountDevice($accountId, $device, $ipClient)
    {           
        $accountLock = $this->accountsLockRepository->findAccountLockByDevice($accountId, $device)
                ->getOneOrNullResult();
        
        if($accountLock == null){
            $accountLock = new ClientAccount();
            $accountLock->lockDevice($accountId, $device, $ipClient);
            $this->accountsLockRepository->save($accountLock);
        }
        
        return [
            'locked'     => true,
            'device'     => $accountLock->getClient(),
            'unlockCode' => $accountLock->getUnlockCode()
        ];
    }
    
    /**
     * 
     * @param int $accountId
     * @param string $device
     * @return type
     */
    public function unlockAccountDevice($accountId, $unlockCode, $ipClient)
    {           
        $accountLock = $this->accountsLockRepository->findAccountLockByunlockCode($accountId, $unlockCode)
                ->getOneOrNullResult();
        
        if($accountLock == null){
            throw new BadRequestHttpException('Incorrect key');
        }
        
        $accountLock->setAccountsUnlock($ipClient);
        $this->accountsLockRepository->save($accountLock);
       
        return true;
    }
    
}
