<?php

namespace AuthBundle\Services;

use AuthBundle\Exception\UserBlockedException;
use AuthBundle\Exception\UserUnauthorizedException;
use AuthBundle\Security\User\WebserviceUser;
use AuthBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Entity\ClientManager;
use AuthBundle\Services\AccountManager;
use Symfony\Component\Routing\Router;
use GuzzleHttp\Client as ClientGuzz;

/**
 * Description of DeviceManager
 *
 * @author NachoBordon
 */
class DeviceManager 
{
    /** @var AccountManager */
    private $accountManager;
    /** @var EntityManager */
    private $em;
    /** @var ClientManager  */
    private $clientManager;
    /** @var \GuzzleHttp\Client  */
    private $clientEmail;
    /** @var boolean */
    private $sendEmailUnlockAccount;
    /** @var Router */
    private $router;
    
    //TODO: ÑAPA */
    private $multiportal;
    private $ws_email_multiportal_uri;
    
    /**
     * 
     * @param EntityManager $entityManager
     * @param ClientManager $clientManager
     * @param AccountManager $accountManager
     * @param \GuzzleHttp\Client $clientEmail
     * @param boolean $sendEmailUnlockAccount
     * @param Router $router
     */
    public function __construct(
        EntityManager $entityManager,
        ClientManager $clientManager,
        AccountManager $accountManager,
        \GuzzleHttp\Client $clientEmail,
        $sendEmailUnlockAccount,
        Router $router,
        $multiportal,
        $ws_email_multiportal_uri
    ){
        $this->em = $entityManager;
        $this->clientManager = $clientManager;
    
        $this->accountManager = $accountManager;
        $this->clientEmail = $clientEmail;
        $this->sendEmailUnlockAccount = $sendEmailUnlockAccount;
        $this->router = $router;
        
        //TODO:  ÑAPA
        $this->multiportal = $multiportal;
        $this->ws_email_multiportal_uri = $ws_email_multiportal_uri;
    }
    
    /**
     * 
     * @param Client $client
     * @throws UserBlockedException
     */
    public function checkLocked(Client $client)
    {        
        if( $client->getLocked() ){
            throw new UserBlockedException(
                'LOCKED_DEVICE',
                sprintf("Device '%s' unauthorized.", $client->getName())
            );
        }
    }
    
    /**
     * 
     * @param Client         $client
     * @param WebserviceUser $user
     * @param string         $email
     * @param string         $locale
     * @throws UserUnauthorizedException
     */
    public function checkAccountLock(Client $client, WebserviceUser $user, $email, $locale) 
    {        
        $accountDevice = $this->accountManager->isLockedAccountDevice( $user->getAccount(), $client->getPublicId());
        $isLocckedAccountDevice = $accountDevice['locked'];
        
        if($isLocckedAccountDevice){
            $this->sendUnlockAccount($email, $locale, $accountDevice['unlockCode'], $user->getUEIgamer(), $user->isNeoxgames());
            throw new UserUnauthorizedException(
                'UNKNOWN_DEVICE',
                sprintf("Device '%s' unauthorized.", $client->getName())
            );
        }
    }
    
    /**
     * 
     * @param Client         $client
     * @param WebserviceUser $user
     * @param string         $ipClient
     * @param string         $email
     * @param string         $locale
     * @throws UserUnauthorizedException
     */
    public function checkUnusualDevice(Client $client, WebserviceUser $user, $ipClient, $email, $locale)
    {     
        $isKnown = $this->accountManager->isKnownDevice( $user->getAccount(), $client->getPublicId(), $ipClient );

        if( $isKnown == false){
            $accountDevice = $this->accountManager->lockAccountDevice($user->getAccount(), $client->getPublicId(), $ipClient);
            $this->sendUnlockAccount($email, $locale, $accountDevice['unlockCode'], $user->getUEIgamer(), $user->isNeoxgames());
 
            throw new UserUnauthorizedException(
                'UNKNOWN_DEVICE',
                sprintf("Device '%s' unauthorized.", $client->getName())
            );
        }
    }
    
    private function sendUnlockAccount($email, $locale, $unlockCode, $UEIgamer, $isNeoxgames)
    {
        $urlValidate = $this->router->generate('get_validatedevice', ['token'=>$unlockCode, 'UEIgamer'=>$UEIgamer], Router::ABSOLUTE_URL);

            if($this->sendEmailUnlockAccount == true){        
            //TODO: ÑAPA TEMPORAL PARA DIFERENCIAR SI ES DE NEOXGAMES O TEXYON
                if( $this->multiportal == true && $isNeoxgames == true ){
                        $client = new ClientGuzz(['base_uri'=> $this->ws_email_multiportal_uri]);
                        $params = [ 'type' => 'unlock_account',
                            'email' => $email,
                            'locale' => $locale,
                            'data' => ['-token-' => $urlValidate]
                        ];
                        $response = $client->post(null, ['form_params'=>$params]);
                        return;
                }
  
                $response = $this->clientEmail->post('send', ['form_params' => [
                        'type' => 'unlock_account',
                        'email' => $email,
                        'locale' => $locale,
                        'data' => ['-token-' => $urlValidate]
                    ]            
                ]);        
        }
    }
    
}
