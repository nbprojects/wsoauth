<?php

namespace AuthBundle\Services;

use GuzzleHttp\Client;

/**
 * TODO: PASAR ESTE MANAGER AL TEXYON MANAGERS
 * 
 * Description of WsUserManager
 *
 * @author NachoBordon
 */
class WsUserManager 
{
    private $wsUserPublic;
    private $wsUserPrivate;
        
    /**
     * 
     * @param Client $wsUserPublic
     * @param Client $wsUserPrivate
     */
    public function __construct(
        Client $wsUserPublic,
        Client $wsUserPrivate
    ) {
        $this->wsUserPublic = $wsUserPublic;
        $this->wsUserPrivate = $wsUserPrivate;        
    }
    
    /**
     * TODO: PASAR A TEXYON MANAGERS
     * 
     * @param type $UEIgamer
     * @return type
     */
    public function getAccountStatus($UEIgamer)
    {          
        $params = [
            'UEIgamer' => $UEIgamer
        ];
        $response = $this->wsUserPrivate->get('account/status', ['query' => $params]);
        $result = $this->getBodyContent($response);
       
        return $result;
    }
    
    /**
     * @deprecated La información de dispositivo y cuenta bloqueada se ha pasado a la BBDD de OAuth
     * 
     * @param string $UEIgamer
     * @param string $device
     * @return type
     */
    public function isKnownDevice($UEIgamer, $device)
    {           
        $params = [
            'UEIgamer' => $UEIgamer, 
            'device'   => $device
        ];
        $response = $this->wsUserPrivate->get('device/isknow', ['query' => $params]);
        $result = $this->getBodyContent($response);
       
        return $result;
    }
    
    /**
     * @deprecated La información de dispositivo y cuenta bloqueada se ha pasado a la BBDD de OAuth
     * 
     * @param string $UEIgamer
     * @param string $device
     * @return type
     */
    public function isLockedAccountDevice($UEIgamer, $device)
    {           
        $params = [
            'UEIgamer' => $UEIgamer, 
            'device'   => $device
        ];
        $response = $this->wsUserPrivate->get('device/islocked', ['query' => $params]);
        $result = $this->getBodyContent($response);
       
        return $result;
    }
    
    /**
     * @deprecated La información de dispositivo y cuenta bloqueada se ha pasado a la BBDD de OAuth
     * 
     * @param string $UEIgamer
     * @param string $device
     * @param string $ipClient
     * @return type
     */
    public function lockAccountDevice($UEIgamer, $device, $ipClient)
    {           
        $params = [
            'UEIgamer' => $UEIgamer, 
            'device'   => $device,
            'ipLock'   => $ipClient
        ];
        $response = $this->wsUserPrivate->post('device/lock', ['form_params' => $params]);
        $result = $this->getBodyContent($response);
       
        return $result;
    }
    
    /**
     * @deprecated La información de dispositivo y cuenta bloqueada se ha pasado a la BBDD de OAuth
     * 
     * @param string $UEIgamer
     * @param string $device
     * @return type
     */
    public function unlockAccountDevice($UEIgamer, $unlockCode, $ipClient)
    {           
        $params = [
            'UEIgamer'   => $UEIgamer, 
            'unlockCode' => $unlockCode,
            'ipClient'   => $ipClient
        ];
        $response = $this->wsUserPrivate->put('device/unlock', ['body' => $params]);
        $result = $this->getBodyContent($response);
       
        return $result;
    }
    
    private function getBodyContent($response)
    {
        $body = $response->getBody();
        return json_decode($body->getContents(), true);        
    }
    
}
