<?php

namespace AuthBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use GuzzleHttp\Client; //ClientInterface
use Texyon\Managers\Lib\UeiManager;


class WebserviceUserProvider implements UserProviderInterface
{
    /** @var Client */
    private $wsUserPrivate;
    /** @var UeiManager */
    private $uei;
    
    /**
     * 
     * @param string $wsUserPrivate
     * @param UeiManager $ueiManager
     */
    public function __construct(
        Client $wsUserPrivate, 
        UeiManager $ueiManager        
    )
    {        
        $this->uei = $ueiManager;        
        $this->wsUserPrivate = $wsUserPrivate;
    }


    public function loadUserByUsername($username)
    {
        // make a call to your webservice here
        $userData = $this->getUserData($username); //['username'=>'user1@texyon.com', 'salt'=>'fda449f9beb08f8d1c695fa572060aca', 'roles'=>['ROLE_USER']];
        // pretend it returns an array on success, false if there is no user

        if ($userData) {
            $password = $userData['password'];
            $username = $userData['username'];
            $salt     = $userData['salt'];
            //$roles    = $userData['status']['roles'];
            $roles    = $userData['roles'];
            $accountStatus = $userData['accountStatus'];
                        
           // $this->checkUserStatus($userData);        
            if(!isset($userData['UEIgamer'])){
                $userData['UEIgamer'] = null;
            }

            return new WebserviceUser($username, $password, $salt, $roles, $userData['accountId'], $userData['UEIgamer'], $accountStatus, $userData['neoxgames']);
        }

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }
        
    private function getUserData($username)
    {            
        $params = ['email' => $username];        
        $response = $this->wsUserPrivate->get("account/credentials", ['query'=>$params]);
        $body = $response->getBody();
        $result = json_decode($body->getContents(), true);
      
        if (isset($result['error'])) {
            return false;
        }
        
        return $result;
    }
    
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'AuthBundle\Security\User\WebserviceUser';
    }
}
