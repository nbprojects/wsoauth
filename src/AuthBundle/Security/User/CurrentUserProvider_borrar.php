<?php


namespace AuthBundle\Security\User;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of CurrentUserProvider
 *
 * @author NachoBordon
 */
class CurrentUserProvider 
{
    private $tokenStorage;
    
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
            
    public function getUser()
    {		      
        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }
        
        return $user;
    }
}
