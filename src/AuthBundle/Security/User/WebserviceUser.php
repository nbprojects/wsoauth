<?php

namespace AuthBundle\Security\User;


use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class WebserviceUser implements UserInterface, EquatableInterface
{
    private $username;
    private $password;
    private $salt;
    private $roles;
    private $account;
    private $UEIgamer;
    private $isNeoxgames;
    private $accountStatus;

    public function __construct($username, $password, $salt, array $roles, $account, $UEIgamer, $accountStatus, $neoxgames=0)
    {
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
        $this->account = $account;
        $this->UEIgamer = $UEIgamer;
        $this->accountStatus = $accountStatus;
        $this->isNeoxgames = (boolean)$neoxgames;
        
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getUsername()
    {
        return $this->username;
    }
    
    function getAccountStatus() {
        return $this->accountStatus;
    }
    
    public function eraseCredentials()
    {
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }
    
    public function getAccount()
    {
        return $this->account;
    }

    function getUEIgamer() {
        return $this->UEIgamer;
    }

    function isNeoxgames() {
        return $this->isNeoxgames;
    }



}