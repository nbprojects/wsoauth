<?php

namespace AuthBundle\Repository;

use Doctrine\ORM\EntityRepository;


class AccessTokenRepository extends EntityRepository
{
    public function count($client, $account)
    {
        $sql = "SELECT COUNT(at) 
                FROM AuthBundle:AccessToken at 
                WHERE at.client = :client
                AND at.account = :account                
                ";
        
        return $this->getEntityManager()
            ->createQuery($sql)
            ->setParameters(['client'=>$client, 'account'=>$account])
            ->getSingleScalarResult();
    }
}
