<?php
namespace AuthBundle\Entity;

use FOS\OAuthServerBundle\Entity\AccessToken as BaseAccessToken;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AuthBundle\Repository\AccessTokenRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AccessToken extends BaseAccessToken
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $client;
    
    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    //private $username;
    
    /**
     *
     * @ORM\Column(name="account_id", type="integer", nullable=true)
     */
    private $account;
    
    
    /**
     * Set username
     *
     * @param string $username
     *
     * @return AccessToken
     */
//    public function setUsername($username)
//    {
//        $this->username = $username;
//
//        return $this;
//    }

    /**
     * Get username
     *
     * @return string
     */
//    public function getUsername()
//    {
//        return $this->username;
//    }
    
    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        if( $this->getUser() ){
            //$this->username = $this->getUser()->getUsername();
            $this->account = $this->getUser()->getAccount();
        }
    }

    /**
     * Set account
     *
     * @param string $account
     *
     * @return AccessToken
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }
}
