<?php

namespace AuthBundle\Entity\Model;

/**
 * Class ClientAccount
 * @package AuthBundle\Entity\Model\ClientAccount
 */
class ClientAccount
{

    /**
     *
     * @param int    $accountId
     * @param string $unlockcode
     * @param string $ipLock
     * @param string $clientId
     *
     * @return $this
     */
    public function setClientAccount($accountId, $unlockcode, $ipLock, $clientId)
    {
        $this->setAccount($accountId);
        $this->setUnlockCode($unlockcode);
        $this->setIpLock($ipLock);
        $this->setClient($clientId);

        return $this;
    }

    /**
     * 
     * @param int  $accountId
     * @param type $device
     * @param type $ipLock
     */
    public function lockDevice($accountId, $device, $ipLock)
    {        
        $unlockCode = $this->generateToken(6);                
                
        $this->setClientAccount($accountId, $unlockCode, $ipLock, $device);        
    }
    
    /**
     *
     * @param string $ipUnlock
     *
     * @return AuthBundle\Entity\ClientAccount
     */
    public function setAccountsUnlock($ipUnlock)
    {
        $this->setIpUnlock($ipUnlock);
        $this->setUnlockDate();

        return $this;
    }

    /**
     * 
     * @param int    $accountId
     * @param string $device
     * @param string $ipClient
     *
     * @return AuthBundle\Entity\ClientAccount
     */
    public function newKnownDevice($accountId, $device, $ipClient)
    {
        $this->lockDevice($accountId, $device, $ipClient);
        $this->setAccountsUnlock($ipClient);
        
        return $this;
    }
    
    /**
     * Generate token
     *
     * @return string
     */
    private function generateToken($lenght=30)
    {
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $random = ""; for($i=0;$i<$lenght;$i++) $random .= substr($str,rand(0,62),1);

        return $random;
    }
}
