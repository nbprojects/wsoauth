<?php

namespace AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AuthBundle\Entity\Model\ClientAccount as ClientAccountModel;

/**
 * ClientAccount
 *
 * @ORM\Entity(repositoryClass="AuthBundle\Repository\ClientAccountRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ClientAccount extends ClientAccountModel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="unlockCode", type="string", length=12)
     */
    private $unlockCode;

    /**
     * @var string
     *
     * @ORM\Column(name="ipLock", type="string", length=255)
     */
    private $ipLock;

    /**
     * @var string
     *
     * @ORM\Column(name="ipUnlock", type="string", length=255, nullable=true)
     */
    private $ipUnlock;

    /**
     * @var string
     *
     * @ORM\Column(name="client_id", type="string", length=255)
     */
    private $client;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lockDate", type="datetime")
     */
    private $lockDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="unlockDate", type="datetime", nullable=true)
     */
    private $unlockDate;

    /**
     *
     * @ORM\Column(name="account_id", type="integer", nullable=true)
     */
    private $account;


    /**
     * @ORM\PrePersist
     */
    public function setLockDate()
    {
        $this->lockDate = new \DateTime();

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unlockCode
     *
     * @param string $unlockCode
     *
     * @return ClientAccount
     */
    public function setUnlockCode($unlockCode)
    {
        $this->unlockCode = $unlockCode;

        return $this;
    }

    /**
     * Get unlockCode
     *
     * @return string
     */
    public function getUnlockCode()
    {
        return $this->unlockCode;
    }

    /**
     * Set ipLock
     *
     * @param string $ipLock
     *
     * @return ClientAccount
     */
    public function setIpLock($ipLock)
    {
        $this->ipLock = $ipLock;

        return $this;
    }

    /**
     * Get ipLock
     *
     * @return string
     */
    public function getIpLock()
    {
        return $this->ipLock;
    }

    /**
     * Set ipUnlock
     *
     * @param string $ipUnlock
     *
     * @return ClientAccount
     */
    public function setIpUnlock($ipUnlock)
    {
        $this->ipUnlock = $ipUnlock;

        return $this;
    }

    /**
     * Get ipUnlock
     *
     * @return string
     */
    public function getIpUnlock()
    {
        return $this->ipUnlock;
    }

    /**
     * Get client
     *
     * @return string
     */
    function getClient() {
        return $this->client;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return ClientAccount
     */
    function setClient($client) {
        $this->client = $client;
        
        return $this;
    }

    /**
     * Get lockDate
     *
     * @return \DateTime
     */
    public function getLockDate()
    {
        return $this->lockDate;
    }

    /**
     * Set unlockDate
     *
     * @return ClientAccount
     */
    public function setUnlockDate()
    {
        $this->unlockDate = new \DateTime();

        return $this;
    }

    /**
     * Get unlockDate
     *
     * @return \DateTime
     */
    public function getUnlockDate()
    {
        return $this->unlockDate;
    }


    /**
     * Set account
     *
     * @param integer $account
     *
     * @return ClientAccount
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return integer
     */
    public function getAccount()
    {
        return $this->account;
    }
}
