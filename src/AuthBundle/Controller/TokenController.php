<?php

namespace AuthBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\FOSRestController;
use AuthBundle\Security\User\WebserviceUser;


class TokenController extends FOSRestController
{
   
       
    /**
     * @View()
     * 
     * @return View
     */
    public function getTokeninfoAction()
    {
        $token = $this->get('security.token_storage')->getToken();

        /* @var $entity  WebserviceUser*/
        $entity = $this->getDoctrine()->getRepository('AuthBundle:AccessToken')->findOneByToken($token->getToken());

         if (!$entity) {
             throw $this->createNotFoundException(
                 'No security token found for id '.$token->getToken()
             );
         }
        
         /* @var $UEImanager \Texyon\Managers\Lib\UeiManager */
        $UEImanager = $this->get('uei_manager');
        $UEIgamer = $UEImanager->getTokenUei( $entity->getAccount() );
         
         
        $data = [
            'UEIgamer' => $UEIgamer,
            'username' => $UEIgamer, //$entity->getUsername(), 
            //'account_id' => $entity->getAccount(),
            'scope' => true, 
            'expires_in' => $entity->getExpiresAt(),             
        ];
        
        return $data;
    }
}
