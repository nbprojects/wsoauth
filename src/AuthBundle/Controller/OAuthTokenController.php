<?php

namespace AuthBundle\Controller;

use FOS\OAuthServerBundle\Controller\TokenController as FosTokenController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use OAuth2\OAuth2ServerException;
use OAuth2\OAuth2;
use AuthBundle\Services\GrantAccess;


/**
 * Class OAuthTokenController
 *
 * {@inheritdoc}
 * @Route("/v2")
 */
class OAuthTokenController extends FosTokenController
{
    /** @var  GrantAccess */
    protected $grantAccess;


    public function __construct(
        OAuth2 $server, 
        GrantAccess $grantAccess
    )
    {
        parent::__construct($server);
        $this->grantAccess = $grantAccess;
    }
    
    
    /**
     * Get access token
     * @param Request $request
     * @return TokenInterface
     *
     * @Route("/token", name="fos_oauth_server_token")
     * @Method({"GET","POST"})
     *
     * @ApiDoc(
     *  section="OAuth",
     *  output="FOS\OAuthServerBundle\Model\TokenInterface",
     *  requirements={
     *      { "name"="client_id", "dataType"="string", "description"="The client application's identifier"},
     *      { "name"="client_secret", "dataType"="string", "description"="The client application's secret"},
     *      { "name"="grant_type", "dataType"="string", "requirement"="refresh_token|authorization_code|password|client_credentials|custom", "description"="Grant type"},
     *  },
     *  parameters={
     *      { "name"="username", "dataType"="string", "required"=false, "description"="User name (for `password` grant type)"},
     *      { "name"="password", "dataType"="string", "required"=false, "description"="User password (for `password` grant type)"},
     *      { "name"="refresh_token", "dataType"="string", "required"=false, "description"="The authorization code received by the authorization server(for `refresh_token` grant type`"},
     *      { "name"="code", "dataType"="string", "required"=false, "description"="The authorization code received by the authorization server (For `authorization_code` grant type)"},
     *      { "name"="scope", "dataType"="string", "required"=false, "description"="If the `redirect_uri` parameter was included in the authorization request, and their values MUST be identical"},
     *      { "name"="redirect_uri", "dataType"="string", "required"=false, "description"="If the `redirect_uri` parameter was included in the authorization request, and their values MUST be identical"},
     *  }
     * )
     */
    public function tokenAction(Request $request)
    {
        //return parent::tokenAction($request);

        try {
            $grantType = $request->query->get('grant_type', $request->request->get('grant_type'));            
            
            if ($grantType === OAuth2::GRANT_TYPE_USER_CREDENTIALS){             
                $this->grantAccess->setServer($this->server);
                //$this->grantAccess->checkAccess($client_id, $username, $password, $locale, $ipClient);
                $this->grantAccess->checkAccess($request);
            }
            
            $response = $this->server->grantAccessToken($request);
            
            return $response;
            
        } catch (OAuth2ServerException $e) {
            return $e->getHttpResponse();
        }
    }
    
    
     
}
