<?php

namespace AuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/client")
 */
class ClientController extends Controller
{
    
    
    /**
     * @Route("/create/device")
     */
    public function createDeviceAction(Request $request)
    {
        try{
            
            $serial = $request->query->get('serial');
            $name = $request->query->get('name');
            $grant_type = ['password','refresh_token'];

            if($serial === null){
                throw new \Exception("The required 'serial' parameter is missing");
            }
            if($name === null){
                throw new \Exception("The required 'name' parameter is missing");
            }

            $clientManager = $this->get('fos_oauth_server.client_manager.default');
            $client = $clientManager->createClient();
            $client->setName($name);
            $client->setSerial($serial);
            $client->setAllowedGrantTypes($grant_type);
            $client->setSecret(null);

            $clientManager->updateClient($client);


            $result = [
                'success' => true,
                'client_id' => $client->getId().'_'.$client->getRandomId(),
                //'client_secret' => $client->getSecret()
            ];


            return new JsonResponse($result);  
        } catch (\Exception $ex) {
            return new JsonResponse(['success' => false, 'message'=> $ex->getMessage()]); 
        }
    }
}
