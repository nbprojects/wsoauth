<?php

namespace AuthBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;


class DeviceController extends FOSRestController
{
    
    /**
     * @QueryParam(name="token", nullable=false, strict=true, allowBlank=false, description="Unlock code")
     * @QueryParam(name="UEIgamer", nullable=false, allowBlank=false, strict=true, description="Unique user identifier.")
     * 
     */
    public function getValidatedeviceAction(Request $request)
    {                  
        try{
            $token = $request->query->get('token');
            $UEIgamer = $request->query->get('UEIgamer');
            $ipClient = $request->getClientIp();

            /* @var $UEImanager \Texyon\Managers\Lib\UeiManager */
            $UEImanager = $this->get('uei_manager');
            $accountId = $UEImanager->getIdGamerByUEI($UEIgamer);

            /* @var $accountslockRepository \AuthBundle\Repository\ClientAccountRepository */
            $accountslockRepository = $this->get('client_account.repository');
            $accountLock = $accountslockRepository->findAccountLockByUnlockCode($accountId, $token)->getOneOrNullResult();

            if($accountLock == null){
                
                return $this->redirect( $this->getParameter('unlock_account_url_error') . "?code=incorrect_token" );                
            }

            $accountLock->setAccountsUnlock($ipClient);
            $accountslockRepository->save($accountLock);          

            return $this->redirect( $this->getParameter('unlock_account_url_success') );
            
        } catch (\Exception $ex) {
            $url = $this->getParameter('unlock_account_url_error') . "?code=request_error";
            
            return $this->redirect($url);
        }
    }
    
}
