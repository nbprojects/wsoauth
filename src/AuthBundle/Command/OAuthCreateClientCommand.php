<?php
namespace AuthBundle\Command;

use FOS\OAuthServerBundle\Entity\ClientManager;
//use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class OAuthCreateClientCommand extends ContainerAwareCommand
{
    const OPTION_REDIRECT_URI = 'redirect-uri';
    const OPTION_GRANT_TYPE = 'grant-type';
    const OPTION_CLIENT_NAME = 'client-name';
    const OPTION_CLIENT_SERIAL = 'client-serial';
    const OPTION_CLIENT_SECRET_KEY = 'secret-key';
    
    /**
     * @var ClientManager
     */
    private $clientManager;

    public function __construct(ClientManager $clientManager)
    {
        parent::__construct();

        $this->clientManager = $clientManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('oauth:client:create')
            ->setDescription('Create a new OAuth client')
            ->addOption(
                self::OPTION_CLIENT_NAME,
                null,
                InputOption::VALUE_REQUIRED,
                'Add a client name'
            )
            ->addOption(
                self::OPTION_CLIENT_SERIAL,
                null,
                InputOption::VALUE_OPTIONAL,
                'Add a client serial'
            )
            ->addOption(
                self::OPTION_CLIENT_SECRET_KEY,
                null,
                InputOption::VALUE_OPTIONAL,
                'If set "no" create client without secret'
            )
            ->addOption(
                self::OPTION_REDIRECT_URI,
                null,
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'If set add a redirect uri'
            )
            ->addOption(
                self::OPTION_GRANT_TYPE,
                null,
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
                'If set add a grant type'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$this->checkName($input->getOption(self::OPTION_CLIENT_NAME));
        
        $client = $this->clientManager->createClient();
        $client->setName($input->getOption(self::OPTION_CLIENT_NAME));
        $client->setSerial($input->getOption(self::OPTION_CLIENT_SERIAL));
        $client->setRedirectUris($input->getOption(self::OPTION_REDIRECT_URI));
        $client->setAllowedGrantTypes($input->getOption(self::OPTION_GRANT_TYPE));
    
        $clientSecret = $input->getOption(self::OPTION_CLIENT_SECRET_KEY);
        if($clientSecret === 'no'){
            $client->setSecret(null);
        }
        
        $this->clientManager->updateClient($client);

        $output->writeln('Client created');
        $output->writeln('client_id='.$client->getId().'_'.$client->getRandomId());
        $output->writeln('client_secret='.$client->getSecret());
    }
    
    protected function checkName($name)
    {
        if (!$name){
            throw new \Exception('Client Name is required');
        }
        
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('AuthBundle:Client')->findOneByName($name);

        if ($entity) {
            throw new \Exception('Client Name already exists');
        }
    }

}
