<?php

namespace AuthBundle\Command;

use Application\ServerBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class OAuthCreateUserCommand extends Command
{
    const USERNAME = 'username';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const ROLE = 'role';
    const PATTERN = '/^[a-zA-Z0-9]+$/';

    private $validRoles = ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'];
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('oauth:user:create')
            ->setDescription('Create a new OAuth user')
            ->addOption(
                self::USERNAME,
                null,
                InputOption::VALUE_REQUIRED,
                'Sets username for user.'
            )
            ->addOption(
                self::PASSWORD,
                null,
                InputOption::VALUE_REQUIRED,
                'Sets password for user.'
            )
            ->addOption(
                self::EMAIL,
                null,
                InputOption::VALUE_REQUIRED,
                'Sets email for user.'
            )
            ->addOption(
                self::ROLE,
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Sets allowed role for user. Can be used multiple times.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->validatePassword($input->getOption(self::PASSWORD));
        $this->validateEmail($input->getOption(self::EMAIL));
        $roles = $this->validateRoles($input->getOption(self::ROLE));

        $user = new User();
        $user->setUsername($input->getOption(self::USERNAME));
        $user->setPassword($input->getOption(self::PASSWORD));
        $user->setEmail($input->getOption(self::EMAIL));
        $user->setRoles($roles);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->echoCredentials($input, $output);
    }

    private function validatePassword($password)
    {
        if (!preg_match(self::PATTERN, $password)) {
            throw new BadCredentialsException(sprintf('Password must contain only numbers and letters.'));
        }
    }

    private function validateEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new BadCredentialsException(sprintf('Email must be a valid value.'));
        }
    }

    private function validateRoles($roles)
    {
        $result = null;
        foreach ($roles as $role) {
            if (in_array($role, $this->validRoles)) {
                $result[] = $role;
            }
        }

        if (!$result) {
            throw new InvalidOptionsException(sprintf('Role(s) must be a valid option.'));
        }

        return $result;
    }

    private function echoCredentials(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('OAuth user has been created...');
        $output->writeln(sprintf('Username: %s', $input->getOption(self::USERNAME)));
        $output->writeln(sprintf('Password: %s', $input->getOption(self::PASSWORD)));
        $output->writeln(sprintf('Roles: %s', json_encode($input->getOption(self::ROLE))));
    }
}