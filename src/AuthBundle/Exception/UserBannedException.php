<?php

namespace AuthBundle\Exception;

use OAuth2\OAuth2ServerException;


/**
 * Description of UserBannedException
 *
 * @author Ignacio
 */
class UserBannedException extends OAuth2ServerException
{
    const ERROR_USER_NOT_VALIDATED = 'user_banned';
    const HTTP_GONE = '410 - Gone';
    protected $message = 'User banned from texyon.';
    

    /**
     * 
     * @param string $errorDescription
     */
    public function __construct( $errorDescription = null)
    {
        if($errorDescription == null){
            $errorDescription = $this->message;
        }
            
        parent::__construct(self::HTTP_GONE, self::ERROR_USER_NOT_VALIDATED, $errorDescription);

    }

    
}
