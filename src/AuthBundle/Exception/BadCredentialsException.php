<?php

namespace AuthBundle\Exception;

use OAuth2\OAuth2ServerException;
use OAuth2\OAuth2;


/**
 * Description of BadCredentialsException
 *
 * @author Ignacio
 */
class BadCredentialsException extends OAuth2ServerException
{
    const ERROR_USER_NOT_VALIDATED = 'bad_credentials';
    protected $message = 'Invalid credentials';
    

    /**
     * 
     * @param string $errorDescription
     */
    public function __construct( $errorCode=null, $errorDescription = null)
    {
        if($errorCode == null){
            $errorCode = self::ERROR_USER_NOT_VALIDATED;
        }
        if($errorDescription == null){
            $errorDescription = $this->message;
        }
            
        parent::__construct(OAuth2::HTTP_BAD_REQUEST, $errorCode, $errorDescription);

    }

    
}
