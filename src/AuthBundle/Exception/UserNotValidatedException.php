<?php

namespace AuthBundle\Exception;

use OAuth2\OAuth2ServerException;
use OAuth2\OAuth2;


/**
 * Description of UserNotValidatedException
 *
 * @author Ignacio
 */
class UserNotValidatedException extends OAuth2ServerException
{
    const ERROR_USER_NOT_VALIDATED = 'user_not_validated';
    const HTTP_PAYMENT_REQUIRED = '402 - Payment Required';
    protected $message = 'User does not validated.';
    

    /**
     * 
     * @param string $errorDescription
     */
    public function __construct( $errorDescription = null)
    {
        if($errorDescription == null){
            $errorDescription = $this->message;
        }
            
        parent::__construct(self::HTTP_PAYMENT_REQUIRED, self::ERROR_USER_NOT_VALIDATED, $errorDescription);

    }

    
}
