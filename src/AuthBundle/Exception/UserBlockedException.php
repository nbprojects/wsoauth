<?php

namespace AuthBundle\Exception;

use OAuth2\OAuth2ServerException;
use OAuth2\OAuth2;


/**
 * Description of UserBlockedException
 *
 * @author Ignacio
 */
class UserBlockedException extends OAuth2ServerException
{
    const ERROR_USER_NOT_VALIDATED = 'user_blocked';
    protected $message = 'User is Blocked.';
    

    /**
     * 
     * @param string $errorDescription
     */
    public function __construct( $errorCode=null, $errorDescription = null)
    {
        if($errorCode == null){
            $errorCode = self::ERROR_USER_NOT_VALIDATED;
        }
            
        if($errorDescription == null){
            $errorDescription = $this->message;
        }
        
        parent::__construct(OAuth2::HTTP_FORBIDDEN, $errorCode, $errorDescription);

    }
            
        
}
