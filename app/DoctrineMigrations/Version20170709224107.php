<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170709224107 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ClientAccount (id INT AUTO_INCREMENT NOT NULL, unlockCode VARCHAR(12) NOT NULL, ipLock VARCHAR(255) NOT NULL, ipUnlock VARCHAR(255) DEFAULT NULL, client_id VARCHAR(255) NOT NULL, lockDate DATETIME NOT NULL, unlockDate DATETIME DEFAULT NULL, account_id INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE AccountsLock');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE AccountsLock (id INT AUTO_INCREMENT NOT NULL, unlockCode VARCHAR(12) NOT NULL COLLATE utf8_unicode_ci, ipLock VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ipUnlock VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, tlcSerial VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, lockDate DATETIME NOT NULL, unlockDate DATETIME DEFAULT NULL, account_id INT DEFAULT NULL) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE ClientAccount');
    }
}
